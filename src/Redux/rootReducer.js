import todos from "../features/todos/todosStateSlice";
import filters from "../features/footer/filtersSlice"
import { combineReducers }  from "redux";

const reducers = {
    todos,
    filters
}
const rootReducer = combineReducers(reducers);

export {rootReducer};