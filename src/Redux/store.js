import { rootReducer } from "./rootReducer";
import { createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
// const fetchMiddleware = storeApi => next => action => {
//     if(typeof action === "function"){
//         return action(storeApi.dispatch, storeApi.getState);
//     }

//     return next(action);
// }

const composedEnhancer = composeWithDevTools(applyMiddleware(thunk));

export default  createStore(rootReducer ,composedEnhancer);