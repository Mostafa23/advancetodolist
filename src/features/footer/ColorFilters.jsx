import {useSelector , useDispatch} from "react-redux";
import { colorFilterUseSelector, COLOR_FILTER } from "./filtersSlice";
export const availableColors = ['green', 'blue', 'orange', 'purple', 'red'];

const ColorFilters = () => {
    const colors = useSelector(colorFilterUseSelector);
    const dispatch = useDispatch()
    const renderedColors = availableColors.map((color) => {
        const checked = colors.includes(color);
        const changeType = checked ? "removed" : "added";
        const handlerChangeColorsFilter = () => {
            dispatch(COLOR_FILTER(changeType,color))
        }
        return (
            <label key={color}>
                <input
                    type="checkbox"
                    name={color}
                    defaultChecked={checked}
                    onChange={handlerChangeColorsFilter}
                />
                <span
                    className="color-block"
                    style={{
                        backgroundColor: color,
                    }}
                ></span>
                {color}
            </label>
        )
    })

    return (
        <div className="filters colorFilters">
            <h5>Filter by Color</h5>
            <form className="colorSelection">{renderedColors}</form>
        </div>
    )
}

export default ColorFilters
