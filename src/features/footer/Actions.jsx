import { useDispatch } from "react-redux";
import {fetchTodosAllCompleted, fetchTodosClearCompleted} from "../todos/todosStateSlice";

export default function Actions() {
    const dispatch = useDispatch();
    function handlerTodosAllCompleted(){
        dispatch(fetchTodosAllCompleted())
    }
    function handlerTodosClearCompleted(){
        dispatch(fetchTodosClearCompleted())
    }
    return (
        <div className="actions">
            <h5>Actions</h5>
            <button className="button" onClick={handlerTodosAllCompleted}>
                Mark All Completed
            </button>
            <button className="button" onClick={handlerTodosClearCompleted}>
                Clear Completed
            </button>
        </div>
    )
}