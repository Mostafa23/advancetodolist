import produce from "immer";

export const statusFilter = {
    All: "ALL",
    Active: "ACTIVE",
    Completed: "COMPLETED"
}

const initState = {
    status: statusFilter.All,
    colors: []
}

export default  produce(filterReducer,initState)

function filterReducer(state, action) {
    switch (action.type) {
        case "filters/STATUS_FILTER":
            state.status = action.payload;
            break;

        case "filters/COLOR_FILTER":
            switch (action.payload.changeType) {
                case "added":
                    if (!state.colors.includes(action.payload.color)) {
                        state.colors.push(action.payload.color);
                    }
                    break;
                case "removed":
                    state.colors = state.colors.filter(c => {
                        if (c === action.payload.color) {
                            return false
                        }
                        return c;
                    })
                    break;
            }
    }
}

// export default function filterReducer(state = initState, action) {
//     switch (action.type) {
//         case "filters/STATUS_FILTER":
//             return {
//                 ...state,
//                 status: action.payload
//             }

//         case "filters/COLOR_FILTER":
//             switch (action.payload.changeType) {
//                 case "added":
//                     if (state.colors.includes(action.payload.color)) {
//                         return state;
//                     }
//                     return {
//                         ...state,
//                         colors: [...state.colors, action.payload.color]
//                     }
//                 case "removed":
//                     return {
//                         ...state,
//                         colors: state.colors.filter(c => {
//                             if (c === action.payload.color) {
//                                 return false
//                             }
//                             return c;
//                         })
//                     }
//                 default:
//                     return state;
//             }

//         default:
//             return state;
//     }
// }



export function STATUS_FILTER(key) {
    return {
        type: "filters/STATUS_FILTER",
        payload: statusFilter[key]
    }
}

export function COLOR_FILTER(changeType,color) {
    return {
        type: "filters/COLOR_FILTER",
        payload: {
            changeType,
            color
        }
    }
}

export function statusFilterUseSelector(state) {
    return state.filters.status;
}

export function colorFilterUseSelector(state){
    return state.filters.colors
}

export function remainTodos(state){
    const remainTodosVar = Object.values(state.todos.entries).filter(todo => {
        return !todo.completed;
    })

    return remainTodosVar.length;
}
