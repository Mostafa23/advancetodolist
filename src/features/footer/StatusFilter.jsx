import { useDispatch, useSelector } from "react-redux"
import { statusFilter, statusFilterUseSelector, STATUS_FILTER } from "./filtersSlice";


const StatusFilter = () => {
    const status = useSelector(statusFilterUseSelector);
    const dispatch = useDispatch();
    const renderedFilters = Object.keys(statusFilter).map((key) => {
        const value = statusFilter[key];
        const className = value === status ? 'selected' : '';

        const handlerStatus = () => {
            dispatch(STATUS_FILTER(key));
        }

        return (
            <li key={value}>
                <button className={className} onClick={handlerStatus}>
                    {key}
                </button>
            </li>
        )
    })

    return (
        <div className="filters statusFilters">
            <h5>Filter by Status</h5>
            <ul>{renderedFilters}</ul>
        </div>
    )
}

export default StatusFilter