import {useState} from "react";
import { useDispatch } from "react-redux";
import { saveNewTodo } from "../todos/todosStateSlice";
import ClipLoader from "react-spinners/ClipLoader";

export default function Header() {
    const [text, setText] = useState("");
    const dispatch = useDispatch();

    const [loading,setLoading] = useState("idle");

    const handlerChange = (e) => setText(e.target.value);

    const handlerKeyDown = async (e) => {
        const trimmedText = text.trim();
        if(e.which === 13 && trimmedText){
            setLoading("loading");
            setText("");
            await dispatch(saveNewTodo(trimmedText));
            setLoading("idle");
        }
    }


    return (
        <header className="header">
            <input
                className="new-todo"
                placeholder={loading === "loading" ? "Please Wait" : 'What needs to be done?'}
                value={text}
                onChange={handlerChange}
                onKeyDown={handlerKeyDown}
                disabled={loading === "loading"}
            />
            {loading === "loading" ? <ClipLoader color={"#6c5ce7"} loading={true} size={20} /> : null}
        </header>
    )
}
