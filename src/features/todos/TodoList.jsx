import TodoListItem from './TodoListItem'
import {  shallowEqual, useSelector } from 'react-redux'
import { SELECTOR } from './todosStateSlice';
import { ClipLoader } from 'react-spinners';
import "./loading.css";

const TodoList = () => {
    const todosIds = useSelector(SELECTOR,shallowEqual);
    const status = useSelector(state => state.todos.status);

    const renderedListItems = todosIds.map((todoId) => {
        return <TodoListItem key={todoId} todoId={todoId} />
    })

    const loader = <div className='container'>
        <ClipLoader color={"#6c5ce7"} loading={true} size={65} />
    </div>
    return (
        <>
        {status === "loading" ? loader : <ul className="todo-list">{renderedListItems}</ul>}
        </>
    )
}

export default TodoList
