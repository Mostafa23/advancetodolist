import produce from "immer";
import { createSelector } from "reselect";
import { del, get, patch, post, put } from "../../services/axios";

const initState = {
    status: "idle",
    entries: {}
}
// const initState = {
//     entries: {
//         1: { id: 1, text: 'Deign ui', completed: true, color: 'red' },
//         2: { id: 2, text: 'discover state', completed: false },
//         3: { id: 3, text: 'discover actions', completed: false },
//         4: { id: 4, text: 'implement reducer', completed: false, color: 'blue' },
//         5: { id: 5, text: 'Complete patterns', completed: false, color: 'red' }
//     }
// }

export default produce(todosReducer, initState)
function todosReducer(state, action) {
    switch (action.type) {
        case "todos/ADDED_TODO":
            const id = action.payload.id;
            state.entries[id] = action.payload;
            break;
        case "todos/TOGGLED_TODO":
            const TOGGLE_ID = action.payload;
            state.entries[TOGGLE_ID].completed = !state.entries[TOGGLE_ID].completed
            break;
        case "todos/DELETED_TODO":
            const DELETED_ID = action.payload;
            delete state.entries[DELETED_ID];
            break;
        case "todos/CHANGE_COLOR":
            const { todoId, color } = action.payload;
            state.entries[todoId].color = color;
            break;
        case "todos/TODOS_ALL_COMPLETED":
            Object.values(state.entries).forEach(todo => {
                state.entries[todo.id].completed = true;
            })
            break;
        case "todos/TODOS_CLEAR_COMPLETED":
            Object.values(state.entries).forEach(todo => {
                if (todo.completed) {
                    delete state.entries[todo.id];
                }
            })
            break;
        case "todos/FETCH_TODOS_LOADING":
            state.status = "loading";
            break;
        case "todos/FETCH_TODOS_LOADED":
            const todos = action.payload;
            const newEntries = {};
            todos.forEach(todo => {
                newEntries[todo.id] = todo;
            })

            state.entries = newEntries;
            state.status = "idle"
            break;
    }
}

// export default function todosReducer(state=initState,action){
//     switch (action.type) {
//         case "todos/ADDED_TODO":
//             const todoId = action.payload.id
//             return {
//                 ...state,
//                 entries : {
//                     ...state.entries,
//                     [todoId] : action.payload
//                 }
//             }

//             case "todos/TOGGLED_TODO":
//                 const TOGGLE_ID = action.payload;
//                 return {
//                     ...state,
//                     entries: {
//                         ...state.entries,
//                         [TOGGLE_ID] : {
//                             ...state.entries[TOGGLE_ID],
//                             completed: !state.entries[TOGGLE_ID].completed
//                         }
//                     }
//                 }
//             case "todos/DELETED_TODO":
//                 const DELETED_ID = action.payload;
//                 const entries = {...state.entries}
//                 delete entries[DELETED_ID]
//                 return {
//                     ...state,
//                     entries
//                 }
//             case "todos/CHANGE_COLOR":
//                 const colorTodoId = action.payload.todoId;
//                 return{
//                     ...state,
//                     entries:{
//                         ...state.entries,
//                         [colorTodoId]:{
//                             ...state.entries[colorTodoId],
//                             color:action.payload.color
//                         }
//                     }
//                 }
//         default:
//             return state;
//     }
// }

export function ADDED_TODO(todo) {
    return {
        type: "todos/ADDED_TODO",
        payload: todo
    }
}


export const TOGGLED_TODO = (todoId) => ({
    type: "todos/TOGGLED_TODO",
    payload: todoId
})

export function DELETED_TODO(todoId) {
    return {
        type: "todos/DELETED_TODO",
        payload: todoId
    }
}

export function CHANGE_COLOR(todoId, color) {
    return {
        type: "todos/CHANGE_COLOR",
        payload: {
            todoId,
            color
        }
    }
}


export function TODOS_ALL_COMPLETED() {
    return {
        type: "todos/TODOS_ALL_COMPLETED"
    }
}

export function TODOS_CLEAR_COMPLETED() {
    return {
        type: "todos/TODOS_CLEAR_COMPLETED"
    }
}

export function FETCH_TODOS_LOADING(){
    return {
        type:"todos/FETCH_TODOS_LOADING"
    }
}

export function TodoListItemUseSelector(state, todoId) {
    return state.todos.entries[todoId];
}

// thunk function
export function saveNewTodo(text) {
    return async function (dispatch) {
        const postBody = { text, completed: false };
        const {data:todo} = await post("todos", postBody);
        dispatch(ADDED_TODO(todo));
    }
}

export function fetchToggleTodo(todoId) {
    return (dispatch, getState) => {
        const { completed } = getState().todos.entries[todoId];
        patch(`todos/${todoId}`, { completed: !completed })
            .then(() => {
                dispatch(TOGGLED_TODO(todoId));
            })
    }
}

export function fetchDeletedTodo(todoId) {
    return function (dispatch) {
        del(`todos/${todoId}`)
            .then(() => {
                dispatch(DELETED_TODO(todoId));
            })
    }
}


export function fetchChangeColor(todoId, color) {
    return (dispatch) => {
        patch(`todos/${todoId}`, { color })
            .then(() => {
                dispatch(CHANGE_COLOR(todoId, color));
            })
    }
}

export function fetchTodosAllCompleted() {
    return (dispatch, getState) => {
        const todos = Object.values(getState().todos.entries);
        let promise = [];
        todos.forEach(todo => {
            if(!todo.completed){
                promise.push(put(`todos/${todo.id}`, { ...todo,completed:true }));
            }
        })
        Promise.all(promise)
        .then(() => {
            dispatch(TODOS_ALL_COMPLETED())
        });
       
    }
}

export function fetchTodosClearCompleted(){
    return (dispatch,getState) => {
        const todos = Object.values(getState().todos.entries);
        let promise = [];
        todos.forEach(todo => {
            if(todo.completed){
                promise.push(del(`todos/${todo.id}`));
            }
        })
        Promise.all(promise)
        .then(() => {
            dispatch(TODOS_CLEAR_COMPLETED())
        });
    }
}


export const fetchTodos = (dispatch) => {
    dispatch(FETCH_TODOS_LOADING());
    get('todos').then(({ data }) => {
        dispatch({
            type: "todos/FETCH_TODOS_LOADED",
            payload: data
        })
    });
}


function checkStatusFilter(status, entriesTodos) {

    const filterStatus = Object.keys(entriesTodos).filter((todoId) => {
        if (status === "ACTIVE" ? !entriesTodos[todoId].completed : entriesTodos[todoId].completed) {
            return true;
        }
        return false;
    })

    return filterStatus;
}

function checkColorFilter(filtredStatusTodos, state) {
    const filteredColorStatusFilter = filtredStatusTodos.filter(todoId => {
        if (state.filters.colors.includes(state.todos.entries[todoId].color)) {
            return true;
        }
        return false;
    })

    return filteredColorStatusFilter;
}

// export function SELECTOR(state) {

//     if (state.filters.colors.length === 0) {
//         if (state.filters.status === "ALL") {

//             return Object.keys(state.todos.entries);

//         }
//         else if (state.filters.status === "ACTIVE") {

//             return checkStatusFilter("ACTIVE", state.todos.entries);

//         }
//         else if (state.filters.status === "COMPLETED") {

//             return checkStatusFilter("COMPLETED", state.todos.entries);

//         }
//     }
//     else {

//         if (state.filters.status === "ALL") {
//             const colorFilterTodos = checkColorFilter(Object.keys(state.todos.entries), state);

//             return colorFilterTodos;
//         }
//         else if (state.filters.status === "ACTIVE") {
//             const ActiveTodos = checkStatusFilter("ACTIVE", state.todos.entries);

//             const colorFilterTodos = checkColorFilter(ActiveTodos, state);

//             return colorFilterTodos;

//         }
//         else if (state.filters.status === "COMPLETED") {
//             const CompletedTodos = checkStatusFilter("COMPLETED", state.todos.entries);

//             const colorFilterTodos = checkColorFilter(CompletedTodos, state);

//             return colorFilterTodos;
//         }
//     }


// }

export const SELECTOR = createSelector(
    state => state.filters.colors,
    state => state.filters.status,
    state => state.todos.entries,
    state => state,
    (colors, status, entries, state) => {
        if (colors.length === 0) {
            if (status === "ALL") {

                return Object.keys(entries);

            }
            else if (status === "ACTIVE") {

                return checkStatusFilter("ACTIVE", entries);

            }
            else if (status === "COMPLETED") {

                return checkStatusFilter("COMPLETED", entries);

            }
        }
        else {

            if (status === "ALL") {
                const colorFilterTodos = checkColorFilter(Object.keys(entries), state);

                return colorFilterTodos;
            }
            else if (status === "ACTIVE") {
                const ActiveTodos = checkStatusFilter("ACTIVE", entries);

                const colorFilterTodos = checkColorFilter(ActiveTodos, state);

                return colorFilterTodos;

            }
            else if (status === "COMPLETED") {
                const CompletedTodos = checkStatusFilter("COMPLETED", entries);

                const colorFilterTodos = checkColorFilter(CompletedTodos, state);

                return colorFilterTodos;
            }
        }
    }
)
