import { useDispatch, useSelector } from 'react-redux'
import { ReactComponent as TimesSolid } from './times-solid.svg'
import { fetchDeletedTodo, fetchToggleTodo, TodoListItemUseSelector,fetchChangeColor } from './todosStateSlice'


export const availableColors = ['green', 'blue', 'orange', 'purple', 'red']
export const capitalize = (s) => s[0].toUpperCase() + s.slice(1)

const TodoListItem = ({ todoId }) => {
    const todo = useSelector((state) => TodoListItemUseSelector(state,todoId));
    const dispatch = useDispatch();
    const { text, completed, color } = todo;
    const colorOptions = availableColors.map((c) => {
        return (
            <option key={c} value={c}>
                {capitalize(c)}
            </option>
        )
    })

    function handlerToggleChange() {
        dispatch(fetchToggleTodo(todo.id));
    }


    function handlerDeleted() {
        dispatch(fetchDeletedTodo(todo.id));
    }

    function handlerChangeColor (e){
        dispatch(fetchChangeColor(todo.id, e.target.value));
    }

    return (
        <li>
            <div className="view">
                <div className="segment label">
                    <input
                        className="toggle"
                        type="checkbox"
                        checked={completed}
                        onChange={handlerToggleChange}

                    />
                    <div className="todo-text">{text}</div>
                </div>
                <div className="segment buttons">
                    <select
                        className="colorPicker"
                        defaultValue={color}
                        style={{ color }}
                        onChange={handlerChangeColor}
                    >
                        <option value=""></option>
                        {colorOptions}
                    </select>
                    <button className="destroy" onClick={handlerDeleted}>
                        <TimesSolid />
                    </button>
                </div>
            </div>
        </li>
    )
}

export default TodoListItem
