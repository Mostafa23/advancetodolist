import axios from "axios";

const myAxios = axios.create({
    baseURL: process.env.REACT_APP_BASE_URL
})

export function get(url, config={}){
    return myAxios.get(url,config);
}

export function post(url,data={},config={}){
    return myAxios.post(url,data,config);
}

export function del(url,config={}){
    return myAxios.delete(url,config);
}

export function patch(url,data={},config={}){
    return myAxios.patch(url,data,config);
}

export function put(url,data={},config={}){
    return myAxios.put(url,data,config);
}